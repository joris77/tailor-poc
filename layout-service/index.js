const http = require('http');
const httpProxy = require('http-proxy');
const Tailor = require('node-tailor');

const proxy = httpProxy.createProxyServer();
const tailor = new Tailor({});

let routeToNextAppAssets = function (req, res, s) {
    let target = s
    return proxy.web(req, res, {target: target}, e => {
        console.error(`Request proxy ${e}`)
    });
}
const requestHandler = (req, res) => {
    if (req.url.includes('_next')) {
        routeToNextAppAssets(req, res, 'http://127.0.0.1:3000')
    } else if (req.url.includes('universal')) {
        routeToNextAppAssets(req, res, 'http://127.0.0.1:3003')
    } else {
        tailor.requestHandler(req, res)
    }
}

const server = http.createServer(requestHandler);
server.listen(8080);