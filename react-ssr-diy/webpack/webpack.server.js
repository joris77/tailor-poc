const path = require('path')

module.exports = {
    target: 'node',
    entry: './src/server/server.js',
    output: {
        filename: 'server.js',
        path: path.resolve(__dirname, '../dist'),
        publicPath: '/'
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                use: [
                    'babel-loader',
                ],
                exclude: /node_modules/
            },
        ],
    },
    resolve:{
        modules: [path.resolve(__dirname, 'src'), 'node_modules']
    },
    devtool: false,
};
