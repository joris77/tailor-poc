const merge = require('webpack-merge');
const webpack = require('webpack')

const common = require('./webpack.client.common.js');
const { getFragments } = require('../src/lib/ssr-webpack')

module.exports = merge(common, {
    entry: getFragments(true),
    devtool: 'inline-source-map',
    plugins: [
        new webpack.HotModuleReplacementPlugin(),
        new webpack.NamedModulesPlugin(),
        new webpack.DefinePlugin({
            'process.env.NODE_ENV': JSON.stringify('development')
        })
    ]
})