const path = require('path')



// Move until here

module.exports = {
    output: {
        filename: '[name].js',
        path: path.resolve(__dirname, '../dist'),
        publicPath: '/'
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                use: [
                    'babel-loader',
                ],
                exclude: /node_modules/
            },
        ],
    },
    resolve:{
        modules: [path.resolve(__dirname, 'src'), 'node_modules']
    }
};
