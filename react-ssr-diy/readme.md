## Getting started
- npm install
- npm run dev
- Open http://localhost:3002/fragments/fragment1

## Production mode
- npm install
- npm run build
- npm start

## References

- https://github.com/glenjamin/ultimate-hot-reloading-example/

- https://github.com/webpack/webpack-dev-middleware/

- https://redux.js.org/docs/recipes/ServerRendering.html

- https://github.com/gaearon/react-hot-loader Says to disable modules.

- https://github.com/glenjamin/webpack-hot-middleware

## Tips 

- When working with intellij settings -> system settings
  - disable safe write
  - enable safe file when idle to x seconds

## TODO

- make fragments dynamic
- getInitialProps
- try to remove module hot code from client maybe with a plugin
- remove hard coded port in ssr-runtime
- react-dom.development.js:11524 Warning: render(): Calling ReactDOM.render() to hydrate server-rendered markup will stop working in React v17. Replace the ReactDOM.render() call with ReactDOM.hydrate() if you want React to attach to the server HTML.
- describe features
- create separate app.js that demonstrates api and server side hot code reloading