import {initFragmentOnClient} from '../../../lib/ssr-runtime'
import Fragment from './fragment'

initFragmentOnClient(Fragment, 'fragment2')


if (module.hot) {
    module.hot.accept('./fragment', () => { initFragmentOnClient(Fragment, 'fragment2') })
}