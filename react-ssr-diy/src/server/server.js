const chokidar = require('chokidar')
const path = require('path')
const express = require('express')
const React = require('react')

const {addSsr} = require('../lib/ssr-runtime')
const {enableReloading} = require('../lib/reloading')

const server = express()

let publicPath = '/'

if (process.env.NODE_ENV !== 'production') {
    const webpackConfig = require('../../webpack/webpack.client.dev')
    publicPath = webpackConfig.output.publicPath
    enableReloading(server, webpackConfig, chokidar.watch('./server'))
} else {
    server.use(express.static('./dist'))
}

addSsr(server, express, publicPath)

const portNumber = 3002
server.listen(portNumber)
console.log(`listening ${portNumber}`)
