
const React = require('react')
const { render } = require('react-dom')
const reactServer = require('react-dom/server')

const renderFragment = (html, preloadedState, fragmentName, bundleLocation) => {
    return `<html>
      <body>
        <div id="${fragmentName}">${html}</div>
        <script>
          window.__PRELOADED_STATE_${fragmentName}__ = ${JSON.stringify(preloadedState).replace(/</g, '\\x3c')}
        </script>
        <script src="${bundleLocation}/${fragmentName}.js"></script>
      </body>
     <html>
    `
}

const initFragmentOnClient = (Fragment, fragmentName) => {
    const preloadedState = window[`__PRELOADED_STATE_${fragmentName}__`]
    const rootElement = document.getElementById(fragmentName)

    render(<Fragment {...preloadedState} />, rootElement)


}

const addSsr = (server, express, publicPath) => {
    const host = 'localhost';
    const port = 3002
    const bundleLocation = `http://${host}:${port}${publicPath}`
    //server.use(`/${bundleFolder}`, express.static(path.join(__dirname, `../../${bundleFolder}`)))

    server.get('/fragments/*', (req, res) => {
        console.log(req.url)
        const fragmentName = req.url.substr(req.url.lastIndexOf('/') + 1)
        let component = require('../client/fragments/' + fragmentName + '/fragment')
        const html = reactServer.renderToString(React.createElement(component))
        res.send(renderFragment(html, {}, fragmentName, bundleLocation))
    })
}

module.exports = { addSsr, initFragmentOnClient }
