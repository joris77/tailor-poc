const path = require('path')
const fs = require('fs')

const getFragments = (isDevelopment) => {
    const fragmentEntries = {}
    const fragments = path.join(process.cwd(), 'src/client/fragments')
    fs.readdirSync(fragments).forEach(function (file) {
        const fileName = file.split('.')

        let fragmentName = fileName[0]
        const fragmentPath = './src/client/fragments/' + fragmentName + '/client'
        console.log(`Adding entry ${fragmentPath}`)

        const entries = [fragmentPath]
        if(isDevelopment) {
            entries.unshift('react-hot-loader/patch', 'webpack-hot-middleware/client')
        }
        fragmentEntries[fragmentName] = entries
    })
    return fragmentEntries;
}

module.exports = {getFragments}
