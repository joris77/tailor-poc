const enableServerSideReloading = (watcher, compiler) => {
    watcher.on('ready', function () {
        watcher.on('all', function () {
            console.log("Clearing /server/ module cache from server")
            Object.keys(require.cache).forEach(function (id) {
                if (/[\/\\]server[\/\\]/.test(id)) delete require.cache[id]
            })
        })
    })

    compiler.plugin('done', function () {
        console.log("Clearing /client/ module cache from server")
        Object.keys(require.cache).forEach(function (id) {
            if (/[\/\\]client[\/\\]/.test(id)) delete require.cache[id]
        })
    })
}

const enableReloading = (server, webpackConfig, watcher) => {
    const webpack = require('webpack')
    const compiler = webpack(webpackConfig)
    server.use(require("webpack-dev-middleware")(compiler, {
        noInfo: true, publicPath: webpackConfig.output.publicPath
    }))
    server.use(require("webpack-hot-middleware")(compiler))

    enableServerSideReloading(watcher, compiler)
}

module.exports = { enableReloading }