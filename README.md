# Proof of concept for tailorjs and server side rendering #

The challenge is to create as simple as possible services that can output fragments. With as much as possible developer friendly.

What is needed for ssr:

- Endpoint for fragment
- Fetch on the server
- Create store/state on server with fetch result
- Render to string the fragment component
- Render fragment component with surrounding markup and reference to bundle.
- Create bundle for fragment component on the server so page sent to client can find it.
- This should be done for every separate fragment.

Challenges:
- Create endpoint for each fragment with sharing as much as possible
- Have hot code reloading
- Keep it simple
- Keep the code for creating an extra fragment as much as possible in one place.
