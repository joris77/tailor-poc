class Fragment2 extends React.Component {
    static async getInitialProps({ req }) {
        const userAgent = req ? req.headers['user-agent'] : navigator.userAgent
        return { userAgent }
    }

    render() {
        return (
            <div>
                Hello World 2
            </div>
        )
    }
}

export default Fragment2