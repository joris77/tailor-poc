class Fragment1 extends React.Component {
    static async getInitialProps({ req }) {
        const userAgent = req ? req.headers['user-agent'] : navigator.userAgent
        return { userAgent }
    }

    render() {
        return (
            <div>
                Hello World 1
            </div>
        )
    }
}

export default Fragment1